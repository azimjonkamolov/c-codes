// Page_53.2.c
// Write an algorithm to find the larger for two numbers
#include<stdio.h>

int main()
{
	int a, b;
	scanf("%d", &a);
	scanf("%d", &b);
	
	if(a>b)
		printf("%d is higher than %d\n", a,b);
	else if(a<b)
		printf("%d is higher than %d\n", b,a);
	else
		printf("They are equal\n");
	
	return 0;
}
