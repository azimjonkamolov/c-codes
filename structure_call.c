// Name: structure call
// Time: 01:06 11.20.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to call struct and some changes

#include<stdio.h>   // THE HEADER
#include<stdlib.h>
#include<string.h>
#include"E:\\5. Advanced C programming\\Codes\\structure.c"

int main()
{
	struct S st1={10, "Tom", 4.2};
	
	st1.ID+=20;	// structure memeber access
	strcpy(st1.name, "alice"); // st1.name="alice"
	st1.name[0]='A';
	
	printf("ID: %d\n", st1.ID);		// Access to structure memembes
	printf("Name: %s\n", st1.name);
	printf("GPA: %.2f\n", st1.GPA);
	
	return 0;
}
