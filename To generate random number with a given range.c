// to generate random number within a given range.c
#include <stdio.h> 
#include <stdlib.h> 
#include <time.h> 

void printR(int lower, int upper, int count) 
{ 
    int i, num; 
    for (i = 0; i < count; i++) 
	{ 
        num = (rand() % (upper - lower + 1)) + lower; 
        printf("%d ", num); 
    } 
} 
int main() 
{ 
    int lower = 50, upper = 100, count = 1; 
    srand(time(0)); 
    printR(lower, upper, count); 
    return 0; 
} 