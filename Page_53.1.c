// Page_53.1.c
//  Write an algorithm for swapping two values
#include<stdio.h>

int main()
{
	int a=10, b=20, temp;
	temp=a;
	a=b;
	b=temp;
	printf("a: %d, b: %d\n", a,b);
	
	return 0;
}
