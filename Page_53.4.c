// Page_53.4.c
// Write an algorithm to print the grade obtained by a sudent using the followin rules.
#include<stdio.h>

int main()
{
	int a;
	scanf("%d", &a);
	if(a>75)
		printf("O");
	else if(a>=60 && a<=75)
		printf("A");
	else if(a>=50 && a<60)
		printf("B");
	else if(a>=40 && a<50)
		printf("C");
	else
		printf("D");
	
	return 0;
}
