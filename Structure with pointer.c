// Name: Advanced C project 1
// Time: 09:26 11.26.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to store students' info by name, age, with getting a number of students.

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct S
{
	char *name;
	char *age;
};

int main()
{
	int n, i;
	printf("Enter for n:");
	scanf("%d", &n);
	struct S **var=(struct S**)malloc(n*sizeof(struct S**));
	
	for(i=0;i<n;i++)
	{
		var[i]=(struct S*)malloc(sizeof(struct S));
		var[i]->name=(char*)malloc(101*sizeof(char));
		var[i]->age=(char*)malloc(101*sizeof(char));
		
	}
	
	for(i=0;i<n;i++)
	{
		scanf("%s", var[i]->name);
		scanf("%s", var[i]->age);
	}
	
	for(i=0;i<n;i++)
	{
		printf("\n%s ", var[i]->name);
		printf("%s", var[i]->age);
	}
	
	return 0;
}