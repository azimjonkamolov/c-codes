// Name: complex number add strprc.c
// Time: 04:47 11.27.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to practice

#include<stdio.h>   // THE HEADER
#include<stdlib.h>

struct complex
{
	double real, imag;
};

int main()
{
	int i;
	struct complex x[3]={{1.2, 2.0}, {-2.2,-0.3}};
	x[2].real=x[0].real+x[1].real;
	x[2].imag=x[0].imag+x[1].imag;
	for(i=0;i<3;i++)
	{
		printf("x[%d]:%.1f+%.1fi\n", i, x[i].real, x[i].imag);
	}
	
	return 0;
}

