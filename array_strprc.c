// Name: array strprc.c
// Time: 04:47 11.27.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to practice

#include<stdio.h>   // THE HEADER
#include<stdlib.h>  // THE HEADER
#include<string.h>

struct S
{
	int ID;
	char name[10];
	double GPA;		
};

int main()
{
	struct S st[3];
	st[0].ID=16012684;
	strcpy(st[0].name,"Tom");
	st[0].GPA=4.2;
	
	st[1]=st[0];
	st[1].name[2]='p';
	
	printf("%d, %s, %.2f\n", st[0].ID, st[0].name, st[0].GPA);
	printf("%d, %s, %.2f\n", st[1].ID, st[1].name, st[1].GPA);
	
	return 0;
}
