// Name: restaurant menu
// Time: 02:46 11.26.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to order food and calcualte the sum, using structure

#include<stdio.h>   // THE HEADER
#include<stdlib.h>  // THE HEADER

struct F        // THE STRUCT NAME
{
	int m;
	int s[3];
	int b;
};

struct F meal;

int main()
{
	int i, sum=0;
	
	scanf("%d", &meal.m);
	sum+=meal.m;
	for(i=0;i<3;i++)
	{
		scanf("%d", &meal.s[i]);
		sum+=meal.s[i];
	}
	scanf("%d", &meal.b);
	sum+=meal.b;
	
	printf("Main dish: %d\n", meal.m);
	for(i=0;i<3;i++)
	{
		printf("Side dish %d: %d\n", i, meal.s[i]);
	}
	printf("Beverage: %d\n", meal.b);
	printf("The sum: %d\n", sum);
	return 0;
}
