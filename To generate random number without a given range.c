// To generate random numbers.c
#include <stdio.h>  // THE HEADER
#include <stdlib.h> // THE HEADER
#include <time.h>   // THE HEADER

int main ()
{
  srand ( time(NULL) );
  printf ("Random Number: %d\n", rand() %100);
  return 0;
}