// Page_53.5.c
// Write an algorithm to find the sum of first N natural numbers
#include<stdio.h>   // THE HEADER

int main()
{
	int n, sum=0, i=1;
	scanf("%d", &n);
	while(i<=n)
	{
		sum=sum+i;
		i=i+1;
	}
	
	printf("The sum: %d\n", sum);
	return 0;
}
