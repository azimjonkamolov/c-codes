// Name: Struct copy.c
// Time: 04:24 11.27.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to practice struct copy

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct S
{
	char name[15];
	int ID;
	double GPA;
}st1, st2;

int main()
{
	st1={"Azim", 16012684, 4.2};
	st2=st1; // COPY HAPPENS HERE
		printf("The name: %s\n", st2.name);
		printf("The ID: %d\n", st2.ID);
		printf("The GPA: %f\n", st2.GPA);
	
	return 0;
}
