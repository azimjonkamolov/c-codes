// Bitwise_operation.c
#include<stdio.h>   // THE HEADER

int main()
{
	int a=2, b=5; // in binary 0010, 0011
	
	printf("AND: %d\n", a&b); // 0
	printf("<<: %d\n", a<<2); // 8 as 1100
	printf(" |: %d\n", a|b); //  2+5 = 7
	
	return 0;
}

